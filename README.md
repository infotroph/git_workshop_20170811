# Notes for a Git demo

## Duration

An hour or so?

## Objectives

* Understand why and when version control is useful
* Setting up Git
* Understand differences between Git-the-software and Git\[Hub|Lab\]-the-services
* Perform basic Git actions
	- make a new repository
 	- obtain your own copy of an existing repository
 	- make and commit changes
 	- look at project history
 	- undo changes
 	- share changes with others
	- branching & merging
	- resolving conflicts
* Be aware of less basic Git actions
	- reverting to previous versions
	- testing (not really Git, but often tightly coupled)
	- permissions and visibility
	- stashing
	- setting up SSH keys <!-- maybe this is too advanced -->
* Navigate GitLab & maybe GitHub too
	- Issue tracking and pull requests
	- Code review
	- Notifications

## Demos

* Setting up git
	```
	git --version
	git config --list
	git config --global user.name "Your name here"
	git config --global user.email "email@example.com"
	git config --list
	```

* Make a new repository

	```
	mkdir demo_repo
	cd demo_repo
	git init
	echo "three <- function(x)x+3" > three.R
	echo "Functions to make numbers bigger" > ReadMe.md
	git status
	git add three.R ReadMe.md
	git commit -m 'First commit, with hopes of many more'
	```

	<!-- Is it confusing to add files in the same step? Would it be clearer to generate content and then initialize a repo around it, or to create a repo and then put content into it? -->
	<!-- I think this is fine. I've never used echo to create and write to a file but it's easy enough to understand what's going on -->

* Cloning a repository
	- Find the page of the repository on GitLab
	- Choose a location where you want to store your local copy

	```
	mkdir localcopy
	cd localcopy
	git clone https://gitlab.com/ErnstS/Example.git
	```


* Make and commit changes

	```
	echo "four <- functon(x)x+4" >> three.R
	git status
	git add three.R
	git commit -m 'add function for adding four'
	git log
	git diff 'HEAD^' HEAD
	git diff master@{1} master
	```
	<!-- added a git status so that they can see what has changed -->

* publish change to GitLab

	<!-- How to do this without bogging everyone down in account creation / configuration details? -->
	<!-- Everyone push to mine, or each their own? -->
	<!-- We make sure everyone has an account before we start and they can simply create a repository online. This can be a bit fiddly so I think it's good to let them do this with us around -->

	- Sign in to GitLab
	- "New Project"
	- give it a name, perhaps "demo_repo"
	- ignore import options
	- follow "existing folder" instructions

	```
	git remote add origin https://gitlab.com/Username/demo_repo.git
	git push -u origin master
	```
	<!-- using https instead of ssh is probably more straightforward -->

	- Reload Gitlab, see your changes
	- Set the default branch to push to

	```
	git branch --set-upstream-to origin/master
	git branch -vv
	```
	
	- Optional but useful: Set the default behaviour for push (nothing, matching, upstream, current or simple)

* Making and using branches

	- Useful to test ideas or for work in progress
	- If you make a new branch, make sure the name is not already in use in the repository!

	```
	git branch branchname
	git checkout branchname
	git add changes.txt
	git commit -m "Made changes in new branch"
	git push origin branchname
	```

	- To merge: checkout the branch you want to merge into the master
	- Try to merge and fix any merge conflicts that arise
	- Then checkout the master and merge the branch into it
	- This way, the master stays clean until you are sure the branches can be merged

	```
	git checkout branch
	git merge master
	[resolve any merge conflicts]
	git checkout master
	git merge --no-ff branch
	```
	<!-- The --no-ff creates a record of who did the merge and when -->

* Test, debug, fix

<!-- Not sure how to do this most effectively...? --> 
<!-- May be easiest to follow http://swcarpentry.github.io/git-novice/04-changes/ -->
<!-- Run three.R, discover 'functon' typo, examine git log, "fix" with s/functon/funciton/, commit -->
<!-- Discover new typo, decide to throw away previous change, git revert -->
<!-- Maybe we can generate a merge conflict in the example repository I made and then show how to resolve it? -->

* Merge conflicts

	- Don't panic
	- often happens when you forget to pull
	- Not inherently a bad thing! Just means a human needs to decide what to do about them.
	- Similarly, a clean merge doesn't gaurantee correctness
	- No shame in resolving outside of git (save your own changes elsewhere, delete, re-clone, merge)
	- Basic pattern: `grep '<<<'`, decide, fix, `git add`.
	- Not marked fixed until you add to index

* Reverting changes

	- Sometimes you make changes that you want to revert
	- You can always revert to a previous commit, or discard any local changes that have not been commited yet

	```
	git commit -m "This is a terrible mistake"
	git reset HEAD~
	[Edit out mistakes]
	git add --all
	git commit -c ORIG_HEAD
	```
	- The second line unstages the changes in your commit but keeps them on disk. This gives you the option of editing files and then the last line commits it again and allows you to edit the message of the original commit.
	- If you want to go back to a previous commit, use 
	```
	git checkout HASH
	```
	where HASH is the hash of the previous commit
	- You can also go back and make a new branch there with
	```
	git checkout -b newbranchname HASH
	```
	- If you want to go back to a previous state and work from there, use revert. This makes new commits that reverses changes made, so you do not lose any commits

	```
	git revert HASH1 HASH2 HASH3
	git revert HEAD-2...HEAD
	```

	- The first line will revert the changes of the commits that are associated with the three hashes
	- The second line will revert the last three commits

* What NOT to track

	- Changing a binary file usually means Git stores an entirely new copy. Note that this includes Word documents and Excel sheets!
	- Sometimes this is fine! But the larger the file and the more often it's edited, the faster the Git repo grows.
	- Deleting a committed file doesn't remove it from history, even for someone who clones the repo after it's deleted.
	- Avoid committing files that can be automatically regenerated
	- Don't commit secrets (access keys, private patient information, scripts you don't have permission to share...)
	- Use gitignore to keep status lists clean and avoid accidentally committing an unwanted file

* VERY brief overview of GitLab features

	- Issue tracking: Can use for TODOs, assigning tasks, discussing possible implementations...
	- Pull/merge requests: Review and discuss code before adding it to a branch/repo. Most projects use this as their gatekeeping step.
	- Wiki: Usually used for documentation of the whole project, but not part of the repository! Chris prefers to document things within the project instead, where they're version-controlled and automatically downloaded when someone clones the project.
	- Automated testing pipelines: No time for this today, but useful. Click "pipelines" in OSR to see an example.

## Resources

### Specific to Git

[Software Carpentry lesson](https://swcarpentry.github.io/git-novice/)

[Karl Broman's minimal Git/GitHub tutorial](http://kbroman.org/github_tutorial/)

[Katie Sylor-Miller's simple, human-readable, slightly profane problem-to solution translator](http://ohshitgit.com)

[Comprehensive Tutorial and Advanced Tips from Atlassian](https://www.atlassian.com/git/tutorials/what-is-version-control)

[Git for humans](https://speakerdeck.com/alicebartlett/git-for-humans) (also available as a book)

[Pro Git](https://git-scm.com/book/en/v2) is probably the most comprehensive, but not necessarily most readable, Git reference book.

### Scientific computing in general

[Best practices for scientific computing](http://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001745) and its more pragmatic follow-up [Good enough practices in scientific computing](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005510)
